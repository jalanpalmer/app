package meta

import (
	"encoding/json"
	"net/http"
)

// To be overridden via -ldflags "-X ..."
var builtAt = ""
var commitHash = ""
var version = "dev"

// TODO: use time.Time, string, int for these fields, and do validation in
// NewHandler, so that invalid values cause startup failure.
type response struct {
	BuiltAt    string `json:"built_at"`
	CommitHash string `json:"commit_hash"`
	Version    string `json:"version"`
}

type Handler struct {
	payload []byte
}

func NewHandler() (*Handler, error) {
	payload, err := json.Marshal(response{
		BuiltAt:    builtAt,
		CommitHash: commitHash,
		Version:    version,
	})
	if err != nil {
		return nil, err
	}
	h := &Handler{payload: payload}
	return h, nil
}

func (h *Handler) Get(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(h.payload)
}
