package repo

import (
	"strconv"

	"gitlab.com/jalanpalmer/app/pkg/structs"
)

type Repo struct{}

func New() (*Repo, error) {
	return &Repo{}, nil
}

func (r *Repo) GetThing(id int) (structs.Thing, error) {
	t := structs.Thing{
		ID:   id,
		Name: "thing " + strconv.Itoa(id),
	}
	return t, nil
}
