package api

import (
	"encoding/json"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/jalanpalmer/app/pkg/structs"
)

type Repo interface {
	GetThing(id int) (structs.Thing, error)
}

type Handler struct {
	Path string
	repo Repo
}

func NewHandler(repo Repo) (*Handler, error) {
	h := &Handler{
		Path: "/things/",
		repo: repo,
	}
	return h, nil
}

func (h *Handler) Get(w http.ResponseWriter, req *http.Request) {
	if req.Method != http.MethodGet {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	idString := strings.TrimPrefix(req.URL.Path, h.Path)

	id, err := strconv.Atoi(idString)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	if id <= 0 {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	thing, err := h.repo.GetThing(id)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	payload, err := json.Marshal(thing)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(payload)
}
