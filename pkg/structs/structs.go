package structs

type Thing struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
