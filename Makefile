GIT_HASH := $(shell git rev-parse --short HEAD)

.PHONY: all test race vet fmt build docker_build

all: test race vet fmt build docker_build

test:
	go test -v ./...

race:
	go test -v -race ./...

vet:
	go vet ./...

fmt:
	gofmt -d .

build:
	go build -v ./cmd/app

docker_build:
	docker build --tag app:${GIT_HASH} .
