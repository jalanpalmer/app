package main

import (
	"log"
	"net/http"
	"time"

	"gitlab.com/jalanpalmer/app/pkg/api"
	"gitlab.com/jalanpalmer/app/pkg/meta"
	"gitlab.com/jalanpalmer/app/pkg/repo"
)

func main() {
	r, err := repo.New()
	if err != nil {
		log.Fatal(err)
	}

	apiHandler, err := api.NewHandler(r)
	if err != nil {
		log.Fatal(err)
	}

	metaHandler, err := meta.NewHandler()
	if err != nil {
		log.Fatal(err)
	}

	mux := http.NewServeMux()
	mux.HandleFunc(apiHandler.Path, apiHandler.Get)
	mux.HandleFunc("/meta", metaHandler.Get)

	server := http.Server{
		Addr:           ":8080",
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1000000,
	}
	log.Fatal(server.ListenAndServe())
}
