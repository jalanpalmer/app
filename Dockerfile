FROM golang:1.11 AS builder
ARG built_at=""
ARG commit_hash=""
ARG version="dev"
WORKDIR /go/src/gitlab.com/jalanpalmer/app
COPY . .
RUN wget --no-verbose -O /usr/local/bin/dep https://github.com/golang/dep/releases/download/v0.5.0/dep-linux-amd64
RUN chmod +x /usr/local/bin/dep
RUN dep ensure -vendor-only
RUN CGO_ENABLED=0 go install -v \
	-ldflags="-X gitlab.com/jalanpalmer/app/pkg/meta.builtAt=${built_at} \
	          -X gitlab.com/jalanpalmer/app/pkg/meta.commitHash=${commit_hash} \
	          -X gitlab.com/jalanpalmer/app/pkg/meta.version=${version}" \
	./...

FROM scratch
COPY --from=builder /go/bin/app /app
EXPOSE 8080
ENTRYPOINT ["/app"]
